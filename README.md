Depth Sense 325, which is a new Time-of-Flight (TOF) sensor, is preferred for 3D Object Scanning.

3D Object scanning constructs three dimensional models using point cloud data. TOF sensor estimates colour value and depth for each pixel in an image. These data points are converted into a point cloud structure which consists of 3D points with associated colour information. The Point Cloud Library (PCL) contains many 3D point cloud processing algorithms and is employed in this project.

The purpose of the project is creating a 3D complete object model by scanning the object from every possible viewpoint. Many images are collected by scanning the object using TOF camera. Each image is converted into unaligned point cloud. The object is segmented in each partial point cloud. After aligning the 3D partial views of the object into a single frame, the complete 3D model is achieved. Finally, a triangular mesh surface is fitted to the complete 3D aligned point cloud. The 3D model is ready for usage of various visualization and modelling programs by converting into a polygonal mesh file.

Temmuz Ustununal

temmuz.ustununal.89@gmail.com