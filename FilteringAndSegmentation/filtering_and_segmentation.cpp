#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/registration/icp.h>

//ConsoleDemo
#ifdef _MSC_VER
#include <windows.h>
#endif

#include <stdio.h>
#include <vector>
#include <exception>

#include <iostream>
#include <fstream>

#include <DepthSense.hxx>

using namespace DepthSense;
using namespace std;

//point cloud class
typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
//point cloud class - End

Context g_context;
DepthNode g_dnode;
ColorNode g_cnode;
AudioNode g_anode;

uint32_t g_aFrames = 0;
uint32_t g_cFrames = 0;
uint32_t g_dFrames = 0;

bool g_bDeviceFound = false;

//int sleepCount = 0;
//ofstream myfile;

ProjectionHelper* g_pProjHelper = NULL;
StereoCameraParameters g_scp;
//ConsoleDemo - End
    
int user_data;
int pcd_number = 0;

//point cloud class
pcl::PointCloud<pcl::PointXYZRGB>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(point_cloud_ptr);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_plane_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_plane(cloud_plane_ptr);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_extract_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_extract(cloud_extract_ptr);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_cluster_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_cluster(cloud_cluster_ptr);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_rmoutliers_ptr (new pcl::PointCloud<pcl::PointXYZRGB>);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb_rmoutliers(cloud_rmoutliers_ptr);
boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));

pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZRGB>);
//point cloud class - End

//ConsoleDemo Functions
/*----------------------------------------------------------------------------*/
// New audio sample event handler
void onNewAudioSample(AudioNode node, AudioNode::NewSampleReceivedData data)
{
    printf("A#%u: %d\n",g_aFrames,data.audioData.size());
	//myfile << "A#" << g_aFrames <<": " << data.audioData.size() << "\n";
    g_aFrames++;
}

/*----------------------------------------------------------------------------*/
// New color sample event handler
bool rec=false;
ColorNode::NewSampleReceivedData colordata;
void onNewColorSample(ColorNode node, ColorNode::NewSampleReceivedData data)
{
    printf("C#%u: %d\n",g_cFrames,data.colorMap.size());
	//myfile << "C#" << g_cFrames <<": " << data.colorMap.size() << "\n";
    g_cFrames++;
	if(!rec){
	colordata=data;
	rec=true;
	}
}

/*----------------------------------------------------------------------------*/
// New depth sample event handler
void onNewDepthSample(DepthNode node, DepthNode::NewSampleReceivedData data)
{
    printf("Z#%u: %d\n",g_dFrames,data.vertices.size());
	//myfile << "Z#" << g_dFrames <<": " << data.vertices.size() << "\n";

    // Project some 3D points in the Color Frame
    if (!g_pProjHelper)
    {
        g_pProjHelper = new ProjectionHelper (data.stereoCameraParameters);
        g_scp = data.stereoCameraParameters;
    }
    else if (g_scp != data.stereoCameraParameters)
    {
        g_pProjHelper->setStereoCameraParameters(data.stereoCameraParameters);
        g_scp = data.stereoCameraParameters;
    }

    int32_t w, h;
	int32_t cw,ch;
    FrameFormat_toResolution(data.captureConfiguration.frameFormat,&w,&h);
	int cx = w/2;
    int cy = h/2;

	Extended2DPoint *p2DPointsExtended = new Extended2DPoint[w*h];
	UV *UVMap = new UV[w*h];
	uint8_t rr, gg, bb;
	int plane_size=0;
	int32_t u, v;

	if(rec)
	{
		FrameFormat_toResolution(colordata.captureConfiguration.frameFormat,&cw,&ch);
		//g_pProjHelper->get2DCoordinates ( p3DPoints, p2DPoints, w*h, CAMERA_PLANE_COLOR);

		for(size_t i = 0; i < w*h; ++i)
		{
			u = i%w;
			v = i/w;
			p2DPointsExtended[i].point.x = u;
			p2DPointsExtended[i].point.y = v;
			p2DPointsExtended[i].depth = data.vertices[i].z;
		}

		g_pProjHelper->getUVMapping (p2DPointsExtended, UVMap, w*h);
	}

    g_dFrames++;

	viewer->removePointCloud("point cloud");
	viewer->removePointCloud("point cloud plane");
	viewer->removePointCloud("point cloud extract");
	viewer->removePointCloud("point cloud cluster");
	viewer->removePointCloud("point cloud rmoutliers");

//Putting points in Point Cloud 
	point_cloud_ptr->height = h;
    point_cloud_ptr->width = w;
    point_cloud_ptr->is_dense = false;
    point_cloud_ptr->points.resize(h*w); 

	cloud_plane_ptr->height = h;
    cloud_plane_ptr->width = w;
    cloud_plane_ptr->is_dense = false;
    cloud_plane_ptr->points.resize(h*w); 

	uint8_t r(255), g(15), b(15);
	int colorPixelRow;
	int colorPixelCol;
	int colorPixelInd;
	int minRange = 32000;
	for (size_t i = 0; i < point_cloud_ptr->points.size (); ++i)
	{
		point_cloud_ptr->points[i].x = data.vertices[i].x;
		point_cloud_ptr->points[i].y = data.vertices[i].y;
		if(data.vertices[i].z == 32002 || data.vertices[i].z == 32001)
		{
			point_cloud_ptr->points[i].x = std::numeric_limits<float>::quiet_NaN();
			point_cloud_ptr->points[i].y = std::numeric_limits<float>::quiet_NaN();
			point_cloud_ptr->points[i].z = std::numeric_limits<float>::quiet_NaN();
		}
		else
		{
	 		point_cloud_ptr->points[i].z = data.vertices[i].z;
			if(minRange > point_cloud_ptr->points[i].z)
			{
				minRange = point_cloud_ptr->points[i].z;
			}
		}

		if(rec && data.uvMap[i].u < 1 && data.uvMap[i].u > 0 && data.uvMap[i].v < 1 && data.uvMap[i].v > 0)
		{
			//f = UVMap[i].v*ch*cw*3 +UVMap[i].u*cw*3;
			colorPixelRow = (int) (data.uvMap[i].v * ((float) ch));
			colorPixelCol = (int) (data.uvMap[i].u * ((float) cw));
			colorPixelInd = (colorPixelRow)*cw + colorPixelCol;
			bb = colordata.colorMap[colorPixelInd*3];  //->blue
			gg = colordata.colorMap[colorPixelInd*3+1];  //->green
			rr = colordata.colorMap[colorPixelInd*3+2];  //->red
			point_cloud_ptr->points[i].r = rr;
			point_cloud_ptr->points[i].g = gg;
			point_cloud_ptr->points[i].b = bb;

			cloud_plane_ptr->points[plane_size].x = point_cloud_ptr->points[i].x;
			cloud_plane_ptr->points[plane_size].y = point_cloud_ptr->points[i].y;
			cloud_plane_ptr->points[plane_size].z = point_cloud_ptr->points[i].z;
			cloud_plane_ptr->points[plane_size].r = point_cloud_ptr->points[i].r;
			cloud_plane_ptr->points[plane_size].g = point_cloud_ptr->points[i].g;
			cloud_plane_ptr->points[plane_size].b = point_cloud_ptr->points[i].b;
			plane_size++;
		}
	}
//Putting points in Point Cloud - End

// Create the filtering object: downsample the dataset using a leaf size of 1cm
	pcl::VoxelGrid<pcl::PointXYZRGB> vg;
	vg.setInputCloud (point_cloud_ptr);
	std::cout << "minRange: " << minRange  << std::endl;
	if(minRange < 1000)
	{
		vg.setLeafSize (5.0f, 5.0f, 5.0f);
	}
	else
	{
		vg.setLeafSize (20.0f, 20.0f, 20.0f);
	}
	vg.filter (*cloud_extract_ptr);
	std::cout << "PointCloud after filtering has: " << cloud_extract_ptr->points.size ()  << " data points." << std::endl;

	//viewer->addPointCloud<pcl::PointXYZRGB> (cloud_extract_ptr, rgb_extract, "point cloud extract");

	// Create the segmentation object for the planar model and set all the parameters
	pcl::SACSegmentation<pcl::PointXYZRGB> seg;
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	seg.setOptimizeCoefficients (true);
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (100);
	seg.setDistanceThreshold (/*34.0*/35.0);

	seg.setInputCloud (cloud_extract_ptr);
	seg.segment (*inliers, *coefficients);

	if (inliers->indices.size () == 0)
	{
		PCL_ERROR ("Could not estimate a planar model for the given dataset.");
	}

	std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;

	// Extract the planar inliers from the input cloud
	pcl::ExtractIndices<pcl::PointXYZRGB> extract;
	extract.setInputCloud (cloud_extract_ptr);
	extract.setIndices (inliers);
	extract.setNegative (false);

	// Get the points associated with the planar surface
	extract.filter (*cloud_plane_ptr);
	std::cout << "PointCloud representing the planar component: " << cloud_plane_ptr->points.size () << " data points." << std::endl;
	
	// Remove the planar inliers, extract the rest
	extract.setNegative (true);
	extract.filter (*cloud_f);
	*cloud_extract_ptr = *cloud_f;

	//viewer->addPointCloud<pcl::PointXYZRGB> (cloud_extract_ptr, rgb_extract, "point cloud extract");

	//Creating the KdTree object for the search method of the extraction
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
	tree->setInputCloud (cloud_extract_ptr);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZRGB> ec;
	ec.setClusterTolerance (20); // 2cm
	ec.setMinClusterSize (100);
	ec.setMaxClusterSize (25000);
	ec.setSearchMethod (tree);
	ec.setInputCloud (cloud_extract_ptr);
	ec.extract (cluster_indices);

	int cluster_size = 0;

	for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		if (it->indices.size() > cluster_size)
		{
			cloud_cluster_ptr->clear();
			for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++)
			{
				cloud_cluster_ptr->points.push_back (cloud_extract_ptr->points[*pit]); //*
			}

			cloud_cluster_ptr->width = cloud_cluster_ptr->points.size ();
			cloud_cluster_ptr->height = 1;
			cloud_cluster_ptr->is_dense = true;
		
			std::cout << "PointCloud representing the Cluster: " << cloud_cluster_ptr->points.size () << " data points." << std::endl;

			cluster_size = cloud_cluster_ptr->points.size ();
		}
	}

	//viewer->addPointCloud<pcl::PointXYZRGB> (cloud_cluster_ptr, rgb_cluster, "point cloud cluster");

	// Create the filtering object
	pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
	sor.setInputCloud (cloud_cluster_ptr);
	sor.setMeanK (/*50*/50);
	sor.setStddevMulThresh (/*1.0*/1.0);
	//sor.setNegative (true);
	sor.filter (*cloud_rmoutliers_ptr);

	//cloud_cluster_ptr->clear();
	//pcl::io::loadPCDFile<pcl::PointXYZRGB> ("capture0001.pcd", *cloud_rmoutliers_ptr);

	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_rmoutliers_ptr, rgb_rmoutliers, "point cloud rmoutliers");

	viewer->spinOnce (/*100*/100); //milliseconds
	//boost::this_thread::sleep (boost::posix_time::microseconds (/*100000*/100000)); //milliseconds

	if(pcd_number<20)
	{
		std::stringstream ss;
		pcl::PCDWriter writer;
		ss << "cloud_object_view_" << pcd_number << ".pcd";
		writer.write<pcl::PointXYZRGB> (ss.str (), *cloud_rmoutliers_ptr, false); //*
		pcd_number++;
	}
    
	// Quit the main loop after 200 depth frames received
    if (g_dFrames == 0)
	{
		g_dFrames = 0;
		//myfile.close();
		g_context.quit();
	}
}

/*----------------------------------------------------------------------------*/
void configureAudioNode()
{
    g_anode.newSampleReceivedEvent().connect(&onNewAudioSample);

    AudioNode::Configuration config = g_anode.getConfiguration();
    config.sampleRate = 44100;

    try 
    {
        g_context.requestControl(g_anode,0);

        g_anode.setConfiguration(config);
        
        g_anode.setInputMixerLevel(0.5f);
    }
    catch (ArgumentException& e)
    {
        printf("Argument Exception: %s\n",e.what());
    }
    catch (UnauthorizedAccessException& e)
    {
        printf("Unauthorized Access Exception: %s\n",e.what());
    }
    catch (ConfigurationException& e)
    {
        printf("Configuration Exception: %s\n",e.what());
    }
    catch (StreamingException& e)
    {
        printf("Streaming Exception: %s\n",e.what());
    }
    catch (TimeoutException&)
    {
        printf("TimeoutException\n");
    }
}

/*----------------------------------------------------------------------------*/
void configureDepthNode()
{
    g_dnode.newSampleReceivedEvent().connect(&onNewDepthSample);

    DepthNode::Configuration config = g_dnode.getConfiguration();
    config.frameFormat = FRAME_FORMAT_QVGA;
    config.framerate = 25;
    config.mode = DepthNode::CAMERA_MODE_CLOSE_MODE;
    config.saturation = true;

    g_dnode.setEnableVertices(true);
	g_dnode.setEnableUvMap(true);

    try 
    {
        g_context.requestControl(g_dnode,0);

        g_dnode.setConfiguration(config);
    }
    catch (ArgumentException& e)
    {
        printf("Argument Exception: %s\n",e.what());
    }
    catch (UnauthorizedAccessException& e)
    {
        printf("Unauthorized Access Exception: %s\n",e.what());
    }
    catch (IOException& e)
    {
        printf("IO Exception: %s\n",e.what());
    }
    catch (InvalidOperationException& e)
    {
        printf("Invalid Operation Exception: %s\n",e.what());
    }
    catch (ConfigurationException& e)
    {
        printf("Configuration Exception: %s\n",e.what());
    }
    catch (StreamingException& e)
    {
        printf("Streaming Exception: %s\n",e.what());
    }
    catch (TimeoutException&)
    {
        printf("TimeoutException\n");
    }

}

/*----------------------------------------------------------------------------*/
void configureColorNode()
{
    // connect new color sample handler
    g_cnode.newSampleReceivedEvent().connect(&onNewColorSample);

    ColorNode::Configuration config = g_cnode.getConfiguration();
    config.frameFormat = FRAME_FORMAT_VGA;
    config.compression = COMPRESSION_TYPE_MJPEG;
    config.powerLineFrequency = POWER_LINE_FREQUENCY_50HZ;
    config.framerate = 25;

    g_cnode.setEnableColorMap(true);

    try 
    {
        g_context.requestControl(g_cnode,0);

        g_cnode.setConfiguration(config);
    }
    catch (ArgumentException& e)
    {
        printf("Argument Exception: %s\n",e.what());
    }
    catch (UnauthorizedAccessException& e)
    {
        printf("Unauthorized Access Exception: %s\n",e.what());
    }
    catch (IOException& e)
    {
        printf("IO Exception: %s\n",e.what());
    }
    catch (InvalidOperationException& e)
    {
        printf("Invalid Operation Exception: %s\n",e.what());
    }
    catch (ConfigurationException& e)
    {
        printf("Configuration Exception: %s\n",e.what());
    }
    catch (StreamingException& e)
    {
        printf("Streaming Exception: %s\n",e.what());
    }
    catch (TimeoutException&)
    {
        printf("TimeoutException\n");
    }
}

/*----------------------------------------------------------------------------*/
void configureNode(Node node)
{
    if ((node.is<DepthNode>())&&(!g_dnode.isSet()))
    {
        g_dnode = node.as<DepthNode>();
        configureDepthNode();
        g_context.registerNode(node);
    }

    if ((node.is<ColorNode>())&&(!g_cnode.isSet()))
    {
        g_cnode = node.as<ColorNode>();
        configureColorNode();
        g_context.registerNode(node);
    }

    if ((node.is<AudioNode>())&&(!g_anode.isSet()))
    {
        g_anode = node.as<AudioNode>();
        configureAudioNode();
        g_context.registerNode(node);
    }
}

/*----------------------------------------------------------------------------*/
void onNodeConnected(Device device, Device::NodeAddedData data)
{
    configureNode(data.node);
}

/*----------------------------------------------------------------------------*/
void onNodeDisconnected(Device device, Device::NodeRemovedData data)
{
    if (data.node.is<AudioNode>() && (data.node.as<AudioNode>() == g_anode))
        g_anode.unset();
    if (data.node.is<ColorNode>() && (data.node.as<ColorNode>() == g_cnode))
        g_cnode.unset();
    if (data.node.is<DepthNode>() && (data.node.as<DepthNode>() == g_dnode))
        g_dnode.unset();
    printf("Node disconnected\n");
}

/*----------------------------------------------------------------------------*/
void onDeviceConnected(Context context, Context::DeviceAddedData data)
{
    if (!g_bDeviceFound)
    {
        data.device.nodeAddedEvent().connect(&onNodeConnected);
        data.device.nodeRemovedEvent().connect(&onNodeDisconnected);
        g_bDeviceFound = true;
    }
}

/*----------------------------------------------------------------------------*/
void onDeviceDisconnected(Context context, Context::DeviceRemovedData data)
{
    g_bDeviceFound = false;
    printf("Device disconnected\n");
}
//ConsoleDemo Functions - End
    
int 
main ()
{
	viewer->setBackgroundColor (0, 0, 0);
	viewer->addPointCloud<pcl::PointXYZRGB> (point_cloud_ptr, rgb, "point cloud");
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_plane_ptr, rgb_plane, "point cloud plane");
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_extract_ptr, rgb_extract, "point cloud extract");
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_cluster_ptr, rgb_cluster, "point cloud cluster");
	viewer->addPointCloud<pcl::PointXYZRGB> (cloud_rmoutliers_ptr, rgb_rmoutliers, "point cloud rmoutliers");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud plane");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud extract");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud cluster");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "point cloud rmoutliers");
	viewer->addCoordinateSystem (1.0/*, "global"*/);
	viewer->initCameraParameters ();
	viewer->camera_.clip[0] = 49.6356; 
	viewer->camera_.clip[1] = 49635.6; 
	viewer->camera_.focal[0] = 0.0497412; 
	viewer->camera_.focal[1] = -0.196849; 
	viewer->camera_.focal[2] = -0.0978747; 
	viewer->camera_.pos[0] = 139.91; 
	viewer->camera_.pos[1] = -724.116; 
	viewer->camera_.pos[2] = -1182.76; 
	viewer->camera_.view[0] = -0.973134; 
	viewer->camera_.view[1] = 0.126391; 
	viewer->camera_.view[2] = -0.192448; 
	viewer->camera_.fovy = 0.523599; 
	viewer->camera_.window_size[0] = 960; 
	viewer->camera_.window_size[1] = 520; 
	viewer->camera_.window_pos[0] = 0; 
	viewer->camera_.window_pos[1] = 0; 
	viewer->updateCamera(); 

//ConsoleDemo Main
	//myfile.open ("example.txt");

    g_context = Context::create("localhost");

    g_context.deviceAddedEvent().connect(&onDeviceConnected);
    g_context.deviceRemovedEvent().connect(&onDeviceDisconnected);

    // Get the list of currently connected devices
    vector<Device> da = g_context.getDevices();

    // We are only interested in the first device
    if (da.size() >= 1)
    {
        g_bDeviceFound = true;

        da[0].nodeAddedEvent().connect(&onNodeConnected);
        da[0].nodeRemovedEvent().connect(&onNodeDisconnected);

        vector<Node> na = da[0].getNodes();
        
        printf("Found %u nodes\n",na.size());
		//myfile << "Found " << na.size() <<" nodes\n";
        
        for (int n = 0; n < (int)na.size();n++)
            configureNode(na[n]);
    }

    g_context.startNodes();

    g_context.run();

    g_context.stopNodes();

	//while (!viewer->wasStopped ())
	//{
	//	g_context.startNodes();

	//	g_context.run();
	//	
	//	viewer->spinOnce (/*100*/100); //milliseconds
	//	boost::this_thread::sleep (boost::posix_time::microseconds (/*100000*/100000)); //milliseconds

	//	g_context.stopNodes();
	//}

    if (g_cnode.isSet()) g_context.unregisterNode(g_cnode);
    if (g_dnode.isSet()) g_context.unregisterNode(g_dnode);
    if (g_anode.isSet()) g_context.unregisterNode(g_anode);

    if (g_pProjHelper)
        delete g_pProjHelper;
//ConsoleDemo Main - End

	//while (!viewer->wasStopped ())
	//{		
	//	viewer->spinOnce (/*100*/100); //milliseconds
	//	boost::this_thread::sleep (boost::posix_time::microseconds (/*100000*/100000)); //milliseconds
	//}

    return 0;
}